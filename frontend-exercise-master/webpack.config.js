const path = require( "path" );

module.exports = {
	mode: "development",
	entry: ["./src/js/app.js","./src/js/index.js"],
	output: {
		path: path.resolve(__dirname, "public"),
		filename: "app.bundle.js"
	}
};
