const express = require('express');
const router = express.Router();

router.get('/einstein-and-churchill-both-took-daily-naps', (req, res) => {
	res.render("home", {
		meta: req.app.get('meta'),
		article: req.app.get('article'),
		comment: req.app.get('comment'),
		posts: req.app.get('posts'),
	});
});
router.get('/', (req, res) => {
	res.render("home", {
		meta: req.app.get('meta'),
		article: req.app.get('article'),
		comment: req.app.get('comment'),
		posts: req.app.get('posts'),
	});
	
});

module.exports = router;
