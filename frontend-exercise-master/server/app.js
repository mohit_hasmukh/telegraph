const express = require('express');
const exphbs  = require('express-handlebars');
const router = require('./router');
const meta = require("./content/meta.json");
const article = require("./content/article.json");
const posts = require("./content/posts.json");
const comment = require("../db.json");
const app = express();
const port = 3000;

var hbs = exphbs.create({
    // Specify helpers which are only registered on this instance.
    helpers: {
        ifCond: function(v1, v2, options) {
            if(v1 === v2) {
              return options.fn(this);
            }
            return options.inverse(this);
          },
        setVar: function(varName, varValue, options) {
            options.data.root[varName] = varValue;
          }
    }
});


app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');
app.set('meta', meta);
app.set('article', article);
app.set('comment', comment);
app.set('posts', posts);

app.use('*/static', express.static('public'));

app.use(router);

app.listen(port, () => console.log(`Listening on port ${port}!`));
