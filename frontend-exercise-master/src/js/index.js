document.addEventListener("DOMContentLoaded", function() {


    var articledateElement = document.getElementById('article-date');
    var elements = document.getElementsByClassName('published-date');
    var articledateAttribute = articledateElement.getAttribute('datetime');

    //article date formatter
    const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    const monthsHalf = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    const days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var date = new Date(articledateAttribute);
    var hours = date.getHours();
    var hours2 = date.getHours() % 12 || 12;
    var hours = (hours + 24 - 2) % 24;
    var mid = 'am';
    if (hours == 0) { //At 00 hours we need to show 12 am
        hours = 12;
    } else if (hours > 12) {
        hours = hours % 12;
        mid = 'pm';
    }

    document.getElementById('article-date').innerHTML = days[date.getDay()] + ' ' + date.getDate() + ' ' + months[date.getMonth()] + ' ' + date.getFullYear() + ', ' + hours2 + ':' + date.getMinutes() + mid;

    //Related stories date formattter
    for (var i = 0; i < elements.length; i++) {
        var commentsDateAttribute = elements[i].getAttribute('datetime');
        var date = new Date(commentsDateAttribute);
        var hours = date.getHours();
        var hours2 = date.getHours() % 12 || 12;
        var hours = (hours + 24 - 2) % 24;
        var mid = 'am';
        if (hours == 0) { //At 00 hours we need to show 12 am
            hours = 12;
        } else if (hours > 12) {
            hours = hours % 12;
            mid = 'pm';
        }

        elements[i].innerHTML = date.getDate() + ' ' + monthsHalf[date.getMonth()] + ' ' + date.getFullYear() + ', ' + hours2 + ':' + date.getMinutes() + mid;
    }




});